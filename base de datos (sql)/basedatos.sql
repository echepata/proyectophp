-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2014 a las 03:54:45
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `basedatos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `busqueda`
--

CREATE TABLE IF NOT EXISTS `busqueda` (
`ID` int(11) NOT NULL,
  `busqueda` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `conteo` int(11) NOT NULL,
  `idUniversidad` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `busqueda`
--

INSERT INTO `busqueda` (`ID`, `busqueda`, `conteo`, `idUniversidad`) VALUES
(1, 'iphone', 12, 1),
(3, 'iphone', 5, 2),
(4, 'iphone', 1, 3),
(5, 'empanadas', 43, 1),
(6, 'pastel', 3, 1),
(7, 'pastel de queso', 12, 1),
(8, 'moto', 1, 1),
(9, 'celular', 9, 1),
(10, 'tablet', 3, 1),
(11, 'tableta', 4, 1),
(12, 'pc', 4, 1),
(13, 'computador', 3, 1),
(14, 'chocolates', 9, 1),
(15, 'calculadora', 3, 1),
(16, 'habitacion', 43, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
`ID` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`ID`, `nombre`, `descripcion`) VALUES
(1, 'Comida', 'Aqui puedes encontrar todo tipo de alimentos, vendidos por tus compañeros de estudio.'),
(2, 'Electrónica', 'Aqui puedes encontrar implementos que tus compañeros ya no usen.'),
(3, 'Vivienda', 'Aqui puedes encontrar ofertas de viviendas en las cercanías de tu universidad.'),
(4, 'Automotores', 'Aqui puedes encontrar vehículos que tus compañeros ya no usen.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadopublicacion`
--

CREATE TABLE IF NOT EXISTS `estadopublicacion` (
`ID` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estadopublicacion`
--

INSERT INTO `estadopublicacion` (`ID`, `estado`) VALUES
(1, 'Disponible'),
(2, 'No Disponible'),
(3, 'Pausado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorito`
--

CREATE TABLE IF NOT EXISTS `favorito` (
`ID` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPublicacion` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `favorito`
--

INSERT INTO `favorito` (`ID`, `idUsuario`, `idPublicacion`) VALUES
(1, 1, 4),
(2, 1, 4),
(3, 1, 3),
(4, 1, 5),
(5, 2, 1),
(6, 3, 4),
(7, 2, 8),
(8, 2, 7),
(9, 3, 10),
(10, 4, 15),
(11, 4, 14),
(12, 4, 2),
(13, 4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotopublicacion`
--

CREATE TABLE IF NOT EXISTS `fotopublicacion` (
`ID` int(11) NOT NULL,
  `idPublicacion` int(11) NOT NULL,
  `urlFoto` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `fotopublicacion`
--

INSERT INTO `fotopublicacion` (`ID`, `idPublicacion`, `urlFoto`) VALUES
(1, 1, 'http://ts1.mm.bing.net/th?&id=HN.608013613951877844&w=300&h=300&c=0&pid=1.9&rs=0&p=0'),
(2, 2, 'http://platinumrepairs.co.za/wp-content/uploads/2012/04/DSCN4446.jpg'),
(3, 3, 'http://thecraftycanvas.com/library/files/2012/09/0B.TI-Nspire_cx_cas.home-screen.jpg'),
(4, 4, 'http://makeameme.org/media/created/loses-virginity-annnndddd.jpg'),
(5, 5, 'http://bimg2.mlstatic.com/iphone-4-ou-4s-com-defeito-quebrado-bloqueado_MLB-F-3455149030_112012.jpg'),
(6, 6, 'http://udanis.com/wp-content/uploads/2013/02/CX_CAS_UDANIS_-3818-516x1024.jpg'),
(7, 7, 'http://ts3.mm.bing.net/th?id=HN.607990438308350810&pid=1.7'),
(8, 8, 'http://bimg2.mlstatic.com/iphone-4-ou-4s-com-defeito-quebrado-bloqueado_MLB-F-3455149030_112012.jpg'),
(9, 9, 'http://ts3.mm.bing.net/th?id=HN.607990438308350810&pid=1.7'),
(10, 10, 'http://blogs.longwood.edu/dalesnm/files/2012/04/virginity_8yp8i_16298.jpg'),
(11, 11, 'http://d3oi2nue850v1c.cloudfront.net/media/uploads/p/l/278617_1-gurujee-potato-chips-pepper.jpg'),
(12, 12, 'http://i.ytimg.com/vi/7BZ2O3ulDVQ/maxresdefault.jpg'),
(13, 13, 'http://housewifeindubai.files.wordpress.com/2010/07/baked-cheese-pastry-2.jpg'),
(14, 14, 'http://4.bp.blogspot.com/-9xpfUImmjYQ/TfNoxKsUVII/AAAAAAAAAHw/uooTo5C9uLs/s0/3moto-Yamaha-R1-tuning.jpg'),
(15, 15, 'http://decorationforlife.com/wp-content/uploads/2012/10/Modern-Bedroom-Design-Ideas-For-Mens-16.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE IF NOT EXISTS `publicacion` (
`ID` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `precio` double NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `idUniversidad` int(11) NOT NULL,
  `idEstado` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`ID`, `titulo`, `descripcion`, `precio`, `idCategoria`, `fechaCreacion`, `idUniversidad`, `idEstado`) VALUES
(1, 'Iphone 4S 32GB como nuevo', 'Vendo Iphone 4S, como nuevo, 3 meses de uso, bandas abiertas.\r\n\r\nCon forro y cargador', 470000, 2, '2014-11-23', 1, 1),
(2, 'Iphone 4S 32GB muy dañado', 'Vendo Iphone 4S, para partes, la pantalla esta destruida.\r\n\r\nPor favor ofertas serias. ', 100000, 2, '2014-11-23', 1, 1),
(3, 'Calculadora TI nSpire CAS', 'Vendo Calcuadora Texxas Instruments nSpire CX CAS. 3 años de uso. La vendo porque me gradué. ', 200000, 2, '2014-11-23', 1, 1),
(4, 'Mi virginidad', 'Ha sido cuidad por mas de 20 años, la tenía reservada para el matrimonio, pero mi novia me dejó. Por favor, ser gentiles. ', 1800000, 2, '2014-11-23', 1, 1),
(5, 'Iphone 4S 32GB muy dañado', 'Vendo Iphone 4S, para partes, la pantalla esta destruida.\r\n\r\nPor favor ofertas serias. ', 100000, 2, '2014-11-23', 1, 1),
(6, 'Calculadora TI nSpire CAS', 'Vendo Calcuadora Texxas Instruments nSpire CX CAS. 3 años de uso. La vendo porque me gradué. ', 200000, 2, '2014-11-23', 1, 1),
(7, 'Mi virginidad', 'Ha sido cuidad por mas de 20 años, la tenía reservada para el matrimonio, pero mi novia me dejo. Por favor, ser gentiles. ', 1800000, 2, '2014-11-23', 1, 1),
(8, 'Iphone 4S 32GB muy dañado', 'Vendo Iphone 4S, para partes, la pantalla esta destruida.\r\n\r\nPor favor ofertas serias. ', 100000, 2, '2014-11-23', 1, 1),
(9, 'Calculadora TI nSpire CAS', 'Vendo Calcuadora Texxas Instruments nSpire CX CAS. 3 años de uso. La vendo porque me gradué. ', 200000, 2, '2014-11-23', 1, 1),
(10, 'Mi virginidad', 'Ha sido cuidad por mas de 20 años, la tenía reservada para el matrimonio, pero mi novia me dejo. Por favor, ser gentiles. ', 1800000, 2, '2014-11-23', 1, 1),
(11, 'Papitas de pimienta, Papos', 'Las mas famosas papitas de los originales, PAPOs. Las mas deliciosas papas con pimienta. Pregunta por nuestras papas de limon. ', 1500, 1, '2014-11-23', 1, 1),
(12, 'Celular Alcatel Idol Mini 8Gb', 'Excelente celular Alcatel One Touch Idol Mini, como nuevo. Nunca se me ha caído desde mi 18vo piso, lo juro. ', 240000, 2, '2014-11-23', 1, 1),
(13, 'Pastel de queso', 'Los unicos pasteles de queso, con limonada cerezada. Mira mis otras publicaciones para otros delicios productos. Acepto pedidos de un dia para otro.', 2000, 1, '2014-11-23', 1, 1),
(14, 'Moto Yamaha R1', 'Excelente moto Yamaha R1, motor 1200cc con 150HP y menos de 20000 km. Modelo 2010. Único dueño', 28990000, 4, '2014-11-23', 1, 1),
(15, 'Habitación barrio Laureles', 'Habitación para mujer, ordenada no fumadora, y estudiosa. Incluye alimentación, 3 comidas todos los días, internet, Lavado de la ropa, baño compartido, cama incluida.', 650000, 3, '2014-11-23', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universidad`
--

CREATE TABLE IF NOT EXISTS `universidad` (
`ID` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `ciudad` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `pais` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `universidad`
--

INSERT INTO `universidad` (`ID`, `nombre`, `latitud`, `longitud`, `ciudad`, `pais`) VALUES
(1, 'Universidad Pontificia Bolivariana', 6.2424, -75.589675, 'Medellín', 'Colombia'),
(2, 'Universidad Eafit', 6.200566, -75.578562, 'Medellín', 'Colombia'),
(3, 'Universidad CES', 6.208537, -75.55327, 'Medellín', 'Colombia'),
(4, 'Universidad San Buenaventura', 6.254739, -75.573228, 'Medellín', 'Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`ID` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `seudonimo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID`, `nombre`, `apellido`, `email`, `seudonimo`, `celular`, `foto`, `contrasena`) VALUES
(1, 'Luis Felipe', 'Duarte', 'led123@hotmail.com', 'led', '3003004972', '', 'led123'),
(2, 'Luis Jaime', 'Nazar', 'lucho123@hotmail.com', 'lucho', '3003004973', '', 'lucho123'),
(3, 'Daniela', 'Lotero', 'dani123@hotmail.com', 'dani', '3003004974', '', 'dani123'),
(4, 'Juan Diego', 'Echeverri', 'diego123@hotmail.com', 'diego', '3003004975', '', 'diego123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedorpublicacion`
--

CREATE TABLE IF NOT EXISTS `vendedorpublicacion` (
`ID` int(11) NOT NULL,
  `idVendedor` int(11) NOT NULL,
  `idPublicacion` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `vendedorpublicacion`
--

INSERT INTO `vendedorpublicacion` (`ID`, `idVendedor`, `idPublicacion`) VALUES
(2, 2, 1),
(3, 3, 2),
(4, 2, 3),
(5, 1, 4),
(6, 1, 5),
(7, 2, 6),
(8, 2, 7),
(9, 4, 8),
(10, 3, 9),
(11, 3, 10),
(12, 2, 11),
(13, 4, 12),
(14, 3, 13),
(15, 2, 14),
(16, 1, 15);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `busqueda`
--
ALTER TABLE `busqueda`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `estadopublicacion`
--
ALTER TABLE `estadopublicacion`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `favorito`
--
ALTER TABLE `favorito`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fotopublicacion`
--
ALTER TABLE `fotopublicacion`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `universidad`
--
ALTER TABLE `universidad`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `vendedorpublicacion`
--
ALTER TABLE `vendedorpublicacion`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `busqueda`
--
ALTER TABLE `busqueda`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `estadopublicacion`
--
ALTER TABLE `estadopublicacion`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `favorito`
--
ALTER TABLE `favorito`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `fotopublicacion`
--
ALTER TABLE `fotopublicacion`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `universidad`
--
ALTER TABLE `universidad`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `vendedorpublicacion`
--
ALTER TABLE `vendedorpublicacion`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
