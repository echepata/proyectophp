<?php
    session_cache_expire(20);
    session_start();
    if(isset($_GET["page"]) && $_GET["page"]!=""){
        $url=$_GET["page"];
    }
    else{
        $url="index.php";
    }
    include "php/model/libreria.php";
    $name = stripslashes($_POST["name"]);
    $surname = stripslashes($_POST["surname"]);
    $username = stripslashes($_POST["username"]);
    $email = stripslashes($_POST["email"]);
    $phone = stripslashes($_POST["phone"]);
    $pass = stripslashes($_POST["pass"]);
    $pass2 = stripslashes($_POST["pass2"]);
    $check = stripslashes($_POST["check"]);

    if($pass2!=""){
        //Registro nuevo
        if($pass==$pass2 && $name!="" && $username!="" && $email!="" && $check=="y"){
            $isRegistered = nuevoUsuario($name,$surname,$phone,$username,$email,$pass);
            if($isRegistered){
                $loginData=loginDataOk($email,$pass);
                if($loginData["loginOk"]==TRUE){
                    $_SESSION["loggedIn"]=TRUE;
                    $_SESSION["userID"]=$loginData["userID"];
                    header("location:$url");
                }
            }
            else{   
                $_SESSION["errorNuevoUsuario"]=TRUE;             
                header("location:login.php?page=$url");
            }
        }
        else{
            $_SESSION["datosInsuficientes"]=TRUE; 
            header("location:login.php?page=$url");
        }
    }
    else{
        //Login
        if($pass!="" && $email!=""){
            $loginData=loginDataOk($email,$pass);
            if($loginData["loginOk"]==TRUE){
                $_SESSION["loggedIn"]=TRUE;
                $_SESSION["userID"]=$loginData["userID"];
                header("location:$url");
            }
            else{
                $_SESSION["badData"]=TRUE;
                header("location:login.php?page=$url");
            }


        }
        else{
            header("location:login.php?page=$url");
        }

    }
    

?>
