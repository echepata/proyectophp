<?php
    session_cache_expire(20);
    session_start();
    if(isset($_SESSION["loggedIn"])){
        unset($_SESSION["loggedIn"]);
    }
    if(isset($_SESSION["userID"])){
        unset($_SESSION["userID"]);
    }
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/login.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>
        
<?php
    require_once "header.php" ;
?>
        <script>
            $("#imgLogoHeader").width(0,0);
        </script>
        <div id="ctnLogin">
            <div id="fondoLogin">
                <img id="logo" src="img/logo.jpg" alt="Logo" />
                <div class="msgError" id="mensaje">Ingrese el correo electrónico relacionado con su cuenta</div>
                <input type="email" value="" placeholder="email" class="loginInputText" id="textEmail" autocomplete="on"/><br>
                <div class="msgError" id="mensaje2"></div>
                <input type="button" value="Enviar" class="loginSubmitBtn" id="submitBtn" />
            </div>
        </div>

<?php
    require_once "footer.php" ;
?>
        
        <script src="js/main.js"></script>
        <script src="js/sendEmail.js"></script>
    </body>
</html>
