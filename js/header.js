function irAHeader(destino){
    switch(destino){
        case 1:
            //Atras
            history.back();
            break;
        case 2:
            //Logo
            location.href = "index.php";
            break;
        case 3:
            //Ayuda
            location.href = "index.php";
            break;
    }
}

var atrasHeader, logoHeader, ayudaHeader;
atrasHeader = $("#btnAtras");
logoHeader = $("#logoHeader");
ayudaHeader = $("#btnAyuda");
atrasHeader.click(function () {
    irAHeader(1);
});
logoHeader.click(function () {
    irAHeader(2);
});
ayudaHeader.click(function () {
    irAHeader(3);
});