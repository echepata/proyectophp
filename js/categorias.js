
function mostrarCategorias(datos){
    
    var html = "";
    var respuesta = $("#categorias");
    if (!datos.error) {
        console.dir(datos.categorias);
        for (i in datos.categorias) {
            html += "<input type='button' value='" + datos.categorias[i]["nombre"] + "' onclick='irACategoria("+datos.categorias[i]["ID"]+")' class='categoria ";
            if(i==0){
                html+="primeraOpcion' />"
            }
            else if(i==datos.conteo-1){
                html+="ultimaOpcion' />"
            }
            else{
                html+="' />"
            }
        }
        respuesta.html(html);
    }
}

function irACategoria(e){
    location.href = "index.php?idC=" + e;
}


function descargarDB(e) {
    var params = {};
    params.cmd = "categorias";
    //params.pagina = numeroPagina.val();
    $.ajax({
        type: "POST",
        url: "php/controller/logica.php",
        data: params,
        dataType: "json"
    })
    .done(function (respuesta) {
        mostrarCategorias(respuesta);
    });

}


descargarDB();

var btnCategoria = $(".categorias");
btnCategoria.click(irACategoria);