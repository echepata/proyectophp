

function mostrarInfoUsuario(datos){
    var name = $("#nombreUsuario");
    var username = $("#seudonimoUsuario");
    var email = $("#emailUsuario");
    var phone = $("#celularUsuario");
    var image = $("#imgUsuario");
    if (!datos.error) {
        name.html(datos.usuario["nombre"] + " " + datos.usuario["apellido"]);
        username.html(datos.usuario["seudonimo"]);
        email.html(datos.usuario["email"]);
        phone.html(datos.usuario["celular"]);
        if (datos.usuario["foto"] != "") {
            image.attr("src", datos.usuario["foto"]);
        }
    }
}

function descargarDB(e) {
    var params = {};
    params.cmd = "infoUsuario";
    params.id = idUsuario;
    $.ajax({
        type: "POST",
        url: "php/controller/logica.php",
        data: params,
        dataType: "json"
    })
    .done(function (respuesta) {
        mostrarInfoUsuario(respuesta);
    });

}





function init(){

    descargarDB();

}  

window.onload = init;