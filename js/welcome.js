

function poblarCombo(datos) {
    var html = "";
    var combo = $("#comboUniversidades");
    if (!datos.error) {
        console.dir(datos.universidades);
        for (i in datos.universidades) {
            html += "<option>" + datos.universidades[i]["nombre"] + "</option> <br> ";
        }
        combo.html(html);
    }
}

function cargarUniversidades(e) {
    var params = {};
    params.cmd = "universidades";
    $.ajax({
        type: "POST",
        url: "php/controller/logica.php",
        data: params,
        dataType: "json"
    })
    .done(function (respuesta) {
        poblarCombo(respuesta);
    });

}

cargarUniversidades();

