function mostrarContenido(datos){
    var mensaje = $("#mensaje");
    var formulario = $("#formulario");
    var html = "";
    if(datos.autenticado==true){
        mensaje.html("Ingrese una nueva contraseña en los campos a continuación");
        html += "<form id='formLogin' action='changePassword.php' method='post'>";
        html += "<input type='password' name='pass' value='' placeholder='contraseña' class='loginInputText' id='textPass' /><br>";
        html += "<input type='password' name='pass2' value='' placeholder='confirmar contraseña' class='loginInputText' id='confirmPass' /><br>";
        html += "<input type='password' name='id' value='"+parameterInUrl("p1")+"' class='loginInputText register' id='confirmPass' /><br>";
        html += "<input type='submit' value='Enviar' class='loginSubmitBtn' id='submitBtn' /><br>";
        html += "</form>";
        formulario.html(html);
    }
    else{
        mensaje.html("Este link no es valido para cambiar la contraseña.");
        html += "<input type='button' value='Ir a login' class='loginSubmitBtn' id='submitBtn' /><br>";
        formulario.html(html);
        $("#submitBtn").click(function () {
            location.href = "login.php";
        })
    }
}



function autenticar(){
    
    var params = {};
    params.cmd = "autenticar";
    params.p1 = parameterInUrl("p1");
    params.p2 = parameterInUrl("p2");
    if (params.p1 != "" && params.p2 != "") {
        $.ajax({
            type: "POST",
            url: "php/controller/logica.php",
            data: params,
            dataType: "json"
        })
        .done(function (respuesta) {
            mostrarContenido(respuesta);
        });
    }
    else{
        var datos = {};
        datos.autenticado = false;
        mostrarContenido(datos);
    }

}

function init(){
    var auth = autenticar();    
}








window.onload = init;