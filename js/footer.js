function irAFooter(destino){
    switch(destino){
        case 1:
            //perfilFooter
            location.href = "profile.php";//?url="+location.href;
            break;
        case 2:
            //favoritosFooter
            location.href = "index.php";//?url="+location.href;
            break;
        case 3:
            //buscarFooter
            location.href = "buscar.php";//?url="+location.href;
            break;
        case 4:
            //categoriasFooter
            location.href = "categorias.php";//?url="+location.href;
            break;
        case 5:
            //venderFooter
            location.href = "formularioProducto.php";//?url="+location.href;
            break;
    }
}

var favoritosFooter, perfilFooter, categoriasFooter, venderFooter, buscarFooter;
favoritosFooter = $("#btnFavoritos");
perfilFooter = $("#btnPerfil");
buscarFooter = $("#btnBuscar");
venderFooter = $("#btnVender");
categoriasFooter = $("#btnCategoria");
favoritosFooter.click(function () {
    irAFooter(2);
});
perfilFooter.click(function () {
    irAFooter(1);
});
buscarFooter.click(function () {
    irAFooter(3);
});
categoriasFooter.click(function () {
    irAFooter(4);
});
venderFooter.click(function () {
    irAFooter(5);
});