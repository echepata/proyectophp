
var paginaActual = 1;

function mostrarPublicaciones(datos) {
    var html = "";
    var respuesta = $("#respuesta");
    if (!datos.error) {
        console.dir(datos.publicaciones);
        for (i in datos.publicaciones) {
            html += "<h2>" + datos.publicaciones[i]["titulo"] + "</h2> <br> ";
            html += "<p>" + datos.publicaciones[i]["descripcion"] + "</p><br> ";
        }
        $("#respuesta").html(html);
    }
}

function mostrarPaginador(datos){
    var paginas = datos.paginas;
    var html = "";
    if(!datos.error){
        if(paginaActual>1){
            html+="<a href='javascript:void(0)' onclick='cambiarPagina(-1)'   class='numeroPaginador'><</a>";
        }
        for(var i=1;i<=paginas;i++){
            if(i==paginaActual){
                html+="<a href='javascript:void(0)' onclick='cambiarPagina("+i+")' class='seleccionado'>"+i+"</a>";
            }
            else{
                html+="<a href='javascript:void(0)' onclick='cambiarPagina("+i+")' class='numeroPaginador'>"+i+"</a>";
                
            }         
        }
        if(paginaActual<paginas){
            html+="<a href='javascript:void(0)' onclick='cambiarPagina(0)' class='numeroPaginador'>></a>";
        }   
        $("#paginador").html(html);
    }
}

function cambiarPagina(pagina){
    switch(pagina){
        case -1:
            paginaActual--;
            break;
        case 0:
            paginaActual++;
            break;
        default:
            paginaActual=pagina;
            break;
    }
    descargarDB();
}

function descargarDB(e) {
    var params = {};
    params.cmd = "listar";
    params.publicacion = busqueda.val();
    params.pagina = paginaActual;
    params.categoria = parameterInUrl("idC");
    //params.pagina = numeroPagina.val();
    $.ajax({
        type: "POST",
        url: "php/controller/logica.php",
        data: params,
        dataType: "json"
    })
    .done(function (respuesta) {
        mostrarPublicaciones(respuesta);
        mostrarPaginador(respuesta);
    });

}

var busqueda = $("#textPublicacion");
var btnDB = $("#btnDB");
var numeroPagina = $("#numeroPagina");
btnDB.click(function () {
    paginaActual = 1;
    descargarDB();
});
//busqueda.onchange(descargarDB);
//busqueda.change(descargarDB);
busqueda.keyup(function () {
    paginaActual = 1;
    descargarDB();
});
descargarDB();