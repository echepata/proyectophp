var btnRegistrar,camposRegistro,formaLogin,confirmarPass,textEmail,errorEmail,submitBtn,linkOlvidada;

function init(){
    submitBtn = $("#submitBtn");
    textEmail = $("#textEmail");
    btnRegistrar = $("#loginRegisterBtn");
    camposRegistro = $(".register");
    formaLogin = $("#formLogin");
    confirmarPass = $("#confirmPass");
    errorEmail = $("#errorEmail");
    linkOlvidada = $(".passForgoten");
    btnRegistrar.click(mostrarRegistro);
    textEmail.change(function () {
        verficarCorreo(textEmail.val());
    });

}

function mostrarRegistro(){
    formaLogin.slideUp(300, function () {
        camposRegistro.show();
        linkOlvidada.hide();
        btnRegistrar.val("Ya tengo una cuenta");
        btnRegistrar.click(ocultarRegistro);
        formaLogin.slideDown();

    });
}
function ocultarRegistro(){
    formaLogin.slideUp(300, function () {
        confirmarPass.val("");
        camposRegistro.hide();
        linkOlvidada.show();
        btnRegistrar.val("No tengo una cuenta");
        btnRegistrar.click(mostrarRegistro);
        formaLogin.slideDown();
    });
    
}

function verficarCorreo(correo){
    if ( checkValidation ( correo ) == true ) {
        submitBtn.css("background-color","#ff6868");
        errorEmail.slideUp();
    }
    else{
        submitBtn.css("background-color", "#D3D3D3");
        errorEmail.slideDown();
    }
}


function checkValidation(formInput) {

    if (stringEmpty(formInput)) {
        return (false);
    } else if (noAtSign( formInput )) {
        return (false);
    } else if (nothingBeforeAt(formInput)) {
        return (false);
    } else if (noLeftBracket(formInput)) {
        return (false);
    } else if (noRightBracket(formInput)) {
        return (false);
    } else if (noValidPeriod(formInput)) {
        return (false);
    } else if (noValidSuffix(formInput)) {
        return (false);
    } else {
        return (true);
    }

    var objType = typeof(formInput.focus);
    if (objType == "object" || objType == "function") {
         formInput.focus();
    }

    return (false);
}

function checkValid (formField) {
}

function stringEmpty (formField) {
    // CHECK THAT THE STRING IS NOT EMPTY
    if ( formField.length < 1 ) {
        return ( true );
    } else {
        return ( false );
    }
}

function noAtSign (formField) {
    // CHECK THAT THERE IS AN '@' CHARACTER IN THE STRING
    if (formField.indexOf ('@', 0) == -1) {
        return ( true )
    } else {
        return ( false );
    }
}

function nothingBeforeAt (formField) {
    // CHECK THERE IS AT LEAST ONE CHARACTER BEFORE THE '@' CHARACTER
    if ( formField.indexOf ( '@', 0 ) < 1 ) {
        return ( true )
    } else {
        return ( false );
    }
}

function noLeftBracket (formField) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN CHECK FOR LEFT BRACKET
    if ( formField.indexOf ( '[', 0 ) == -1 && formField.charAt (formField.length - 1) == ']') {
        return ( true )
    } else {
        return ( false );
    }
}

function noRightBracket (formField) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN CHECK FOR RIGHT BRACKET
    if (formField.indexOf ( '[', 0 ) > -1 && formField.charAt (formField.length - 1) != ']') {
        return ( true );
    } else {
        return ( false );
    }
}

function noValidPeriod (formField) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN WE ARE NOT INTERESTED
    if (formField.indexOf ( '@', 0 ) > 1 && formField.charAt (formField.length - 1 ) == ']')
        return ( false );

    // CHECK THAT THERE IS AT LEAST ONE PERIOD IN THE STRING
    if (formField.indexOf ( '.', 0 ) == -1)
        return ( true );

    return ( false );
}

function noValidSuffix(formField) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN WE ARE NOT INTERESTED
    if (formField.indexOf('@', 0) > 1 && formField.charAt(formField.length - 1) == ']') {
        return ( false );
    }

    // CHECK THAT THERE IS A TWO OR THREE CHARACTER SUFFIX AFTER THE LAST PERIOD
    var len = formField.length;
    var pos = formField.lastIndexOf ( '.', len - 1 ) + 1;
    if ( ( len - pos ) < 2 || ( len - pos ) > 4 ) {
        return ( true );
    } else {
        return ( false );
    }
}




window.onload = init;