<?php
    session_cache_expire(20);
    session_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/login.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>
        
<?php
    require_once "header.php" ;
?>
        <script>
            $("#imgLogoHeader").width(0,0);
        </script>
        <div id="ctnLogin">
            <div id="fondoLogin">
                <img id="logo" src="img/logo.jpg" alt="Logo" />
                <form id="formLogin" action="doLogin.php<?php
                            if(isset($_GET["page"])){
                                echo "?page=".$_GET["page"];
                            }
                        ?>" method="post">
                    <?php
                        if($_SESSION["badData"]){
                            echo "<p class='msgError'>El correo o la contraseña estan erradas</p>";
                            unset($_SESSION["badData"]);
                        }
                        if($_SESSION["errorNuevoUsuario"]){
                            echo "<p class='msgError'>Hubo un error al crear este nuevo usuario. Es posible que el correo específicado ya esté registrado o que el seudonimo elegido ya este en uso</p>";
                            unset($_SESSION["errorNuevoUsuario"]);
                        }
                        if($_SESSION["datosInsuficientes"]){
                            echo "<p class='msgError'>Faltaron datos por completar. Los datos con * son obligatorios</p>";
                            unset($_SESSION["datosInsuficientes"]);
                        }
                    ?>
                    
                    <input type="text" name="name" value="" placeholder="* nombres *" class="loginInputText register" /><br class="register">
                    <input type="text" name="surname" value="" placeholder="apellidos" class="loginInputText register" /><br class="register">
                    <input type="tel" name="phone" value="" placeholder="teléfono" class="loginInputText register" /><br class="register">
                    <p class="msgError register">Este telefono se le proporcionará a las personas que compren tus productos</p>
                    <input type="text" name="username" value="" placeholder="* seudonimo *" class="loginInputText register" /><br class="register">
                    <p class="msgError register">Este es el nombre de usuario que aparecera en tus publicaciones</p>
                    <input type="email" name="email" value="" placeholder="* email *" class="loginInputText" id="textEmail" autocomplete="on"/><br>
                    <p class="msgError" id="errorEmail">El correo debe tener la forma de "usuario@dominio.ext"</p>
                    <input type="password" name="pass" value="" placeholder="* contraseña *" class="loginInputText" id="textPass" /><br>
                    <a href="sendEmail.php" class="msgError passForgoten">Olvidé mi contraseña</a><br class="passForgoten">
                    <input type="password" name="pass2" value="" placeholder="confirmar contraseña" class="loginInputText register" id="confirmPass"/><br class="register">
                    <input type="checkbox" name="check" value="y" class="register loginCheckBox" /> <span class="register ">* Acepto las condiciones de uso *</span> <br class="register">
                    <input type="submit" value="Enviar" class="loginSubmitBtn" id="submitBtn" /><br>
                    <input type="button" value="No tengo una cuenta" class="loginSubmitBtn" id="loginRegisterBtn"/>
                </form> 
            </div>
        </div>

<?php
    require_once "footer.php" ;
?>
        
        <script src="js/login.js"></script>
    </body>
</html>
