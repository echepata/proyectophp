<?php
    
    include '../model/libreria.php';
    
    function listar($datos){
        buscarPublicaciones($datos);
    }
    function universidades($datos){
        listarUniversidades($datos);
    }
    function categorias($datos){
        listarCategorias($datos);
    }
    function infoUsuario($datos){
        getInfoUsuario($datos);
    }
    function autenticar($datos){
        autenticarUsuario($datos);
    }
    function verificar($datos){
        verificarCorreo($datos);
    }


    function procesar($datos){
        switch ($datos["cmd"]) {
        case 'listar':
            listar($datos);
            break;
        case 'universidades':
            universidades($datos);
            break;
        case 'categorias':
            categorias($datos);
            break;
        case 'infoUsuario':
            infoUsuario($datos);
            break;
        case 'autenticar':
            autenticar($datos);
            break;
        case 'verificar':
            verificar($datos);
            break;


        default:
            break;
        }
    }
    procesar($_REQUEST);
?>