<?php
function connect_db_local()
{
    $url = "localhost";
    $nombre_base_datos = "basedatos";
    $usuario = "root";
    $password = "";
    $conexion = mysql_connect($url,$usuario,$password);
    mysql_select_db($nombre_base_datos);
    if(!$conexion)
    {
        echo "No pudo conectarse a la BD: " . mysql_error();
        exit;
    }
}
	
function connect_db_server()
{
    $url = "pdb19.biz.nf:3306";
    $nombre_base_datos = "1520187_db";
    $usuario = "1520187_db";
    $password = "miContrasena1";
    $conexion = mysql_connect($url,$usuario,$password);
    mysql_select_db($nombre_base_datos);
    if(!$conexion)
    {
        echo "No pudo conectarse a la BD: " . mysql_error();
        exit;
    }
}
	
function execute_query($query)
{
	connect_db_server();
	$ans = mysql_query($query);
	if(!$ans)
	{
	    return null;
	}
	return $ans;
}

function crearRegistros($result){
    $datos = array();
    while ($fila = mysql_fetch_array($result)) 
    {
        $temp = array();
        foreach ($fila as $key => $value) {
            if(!is_numeric($key)) {
                //if($key == "descripcion" || $key == "titulo") {
                    $value = utf8_encode($value);
                //}
                $temp[$key] = $value;
            }
        }
        array_push($datos, $temp);
    }
    return $datos;
}

function getInfoUsuario($datos){
    $id = $datos["id"];
    $sql = "SELECT * FROM usuario WHERE id=$id ;";
    $result = execute_query($sql);
    $ans["error"] = utf8_encode( $result?false:true);
    $temp = crearRegistros($result);
    $ans["usuario"] = $temp[0];
    echo json_encode($ans);
}

function listarPublicaciones($datos)
{
    $sql = "SELECT * FROM publicacion;";
    $result = execute_query($sql);
    $ans["error"] = $result?false:true;
    $ans["publicaciones"] = crearRegistros($result);
    echo json_encode($ans);
}

function autenticarUsuario($datos)
{
    $idUsuario = $datos["p1"];
    $passUsuario = $datos["p2"];
    $sql = "SELECT contrasena FROM usuario WHERE id=$idUsuario;";
    $result = execute_query($sql);
    $error = $result?false:true;
    if($error==FALSE){
        $temp = mysql_fetch_row($result);
        $passDB = utf8_encode($temp[0]);
        $passDBmd5 = md5($passDB);
        if ( $passDBmd5 == $passUsuario ) {
            $ans["autenticado"] = TRUE;    
        }
        else{
            $ans["autenticado"] = FALSE;   
        }
    }
    else{
        $ans["autenticado"] = FALSE;   
    }
    echo json_encode($ans);
}
function verificarCorreo($datos)
{
    $email = $datos["email"];
    $sql = "SELECT * FROM usuario WHERE email='$email';";
    $result = execute_query($sql);
    $ans["error"] = $result?false:true;
    $temp = crearRegistros($result);
    $temp = $temp[0];
    $id = $temp["ID"];
    $name = $temp["nombre"];
    $surnmae = $temp["apellido"];
    if($id!=""){
        $pass = md5($temp["contrasena"]);
        $url = "http://echepata.co.nf/proyectoPHPFinal/recoverPassword.php?p1=$id&p2=$pass";

        $to = $email; 
        $subject = "Password reset request"; 
        $body = $url; 
        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=iso-8859-1";
        $headers[] = "To: $name $surname<$email>";
        $headers[] = "From: APP IT Services <admin@echepata.co.nf>";
        $headers[] = "Reply-To: APP IT Services <juaneche@msn.com>";
        $headers[] = "Subject: {$subject}";
        $headers[] = "X-Mailer: PHP/".phpversion();
        $headers[] = "Return-Path:<admin@echepata.co.nf>";
        if(mail($to, $subject, $body, implode("\r\n", $headers))){
            echo "Envié el correo\n";
        }
        else{
            echo "No envíe el correo\n";
        }

    }
    else{
        echo "No se encontró el correo en la dB";
    }
    echo "Si el correo se encuentra en nuestra base de datos se le enviara un correo con un link que le permitirá cambiar su contraseña";
}

function listarUniversidades($datos)
{
    $sql = "SELECT * FROM universidad;";
    $result = execute_query($sql);
    $ans["error"] = utf8_encode( $result?false:true);
    $ans["universidades"] = crearRegistros($result);
    echo json_encode($ans);
}
function listarCategorias($datos)
{
    $sql = "SELECT * FROM categoria ORDER BY nombre;";
    $result = execute_query($sql);
    $ans["error"] = utf8_encode( $result?false:true);
    $ans["categorias"] = crearRegistros($result);
    $sql = "SELECT count(*) FROM categoria;";
    $result = execute_query($sql);
    $conteo = mysql_fetch_row($result);
    $ans["conteo"] = utf8_encode($conteo[0]);
    echo json_encode($ans);
}

function buscarPublicaciones($datos){
    $publicacion = $datos["publicacion"];
    $pagina = (int)$datos["pagina"];
    $publicacionesPorPagina = 3;
    $idCategoria = (int)$datos["categoria"];
    $sqlCategoria = $idCategoria?" AND idCategoria=".$idCategoria." ":" ";

    $sql = "SELECT * FROM publicacion WHERE titulo LIKE '%".$publicacion."%'".$sqlCategoria."ORDER BY titulo  LIMIT ".(($pagina-1)*$publicacionesPorPagina).",".$publicacionesPorPagina.";";
    $result = execute_query($sql);
    $ans["error"] = utf8_encode( $result?false:true);
    $ans["publicaciones"] = crearRegistros($result);
    $sql = "SELECT count(*) FROM publicacion WHERE titulo LIKE '%".$publicacion."%'".$sqlCategoria.";";
    $result = execute_query($sql);
    $conteo = mysql_fetch_row($result);
    $paginas = ceil($conteo[0]/$publicacionesPorPagina);
    $ans["paginas"] = utf8_encode($paginas);
    echo json_encode($ans);
}

function loginDataOK($email,$pass){
    $pass = md5($pass);
    $sql = "SELECT * FROM usuario WHERE email='$email' AND contrasena='$pass';";
    $result = execute_query($sql);
    $conteo = mysql_num_rows($result);
    $ans["loginOk"] = $conteo?TRUE:FALSE;
    if($conteo==1){
        $data=mysql_fetch_array($result);
        $ans["userID"]=$data["ID"];
    }
    return $ans;
}

function nuevoUsuario($name,$surname,$phone,$username,$email,$pass){
    $pass = md5($pass);
    $sql = "INSERT INTO usuario (ID, nombre, apellido, email, seudonimo, celular, foto, contrasena) VALUES (NULL, '$name', '$surname', '$email', '$username', '$phone', '', '$pass');";
    //$sql = "SELECT * FROM usuario WHERE email='$email' AND contrasena='$pass';";
    $result = execute_query($sql);
    return $result;
}

function cambiarPassword($id,$pass){
    $pass = md5($pass);
    $sql = "UPDATE usuario SET contrasena='$pass' WHERE id=$id ;";
    $result = execute_query($sql);
    return $result;
}

?>