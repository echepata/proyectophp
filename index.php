<?php
    session_cache_expire(20);
    session_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/paginador.css">
        <link rel="stylesheet" href="css/index.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>
        
<?php
    require_once "header.php" ;
?>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <a href="footer.php">footer</a>
        <a href="header.php">header</a>
        <a href="vista.php">vista</a>
        <a href="vista2.php">vista2</a>
        <a href="profile.php">profile</a>
        <a href="buscar.php">buscar</a>
        <a href="login.php">login</a>
        <a href="welcome.php">welcome</a>
        <a href="recoverPassword.php">recover</a>
        <br>
        <input type="text" placeholder="Buscar un producto" id="textPublicacion" /><input type="button" value="Buscar" id="btnDB"/><br>
        <div id="respuesta"></div>
        <div id="paginador">

        </div>
<?php
    require_once "footer.php" ;
?>
        
        <script src="js/main.js"></script>
        <script src="js/index.js"></script>
    </body>
</html>
