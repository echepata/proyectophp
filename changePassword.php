<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/login.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>
<?php
    
    function mostrarError($msg){
        require_once "header.php" ;
        echo "<h1 style='color: #fff;'>$msg</h1>";
        require_once "footer.php";
    }
    
    function alert($msg){
        $html = "";
        $html .= "<script>";
        $html .= "alert('$msg');";
        $html .= "</script>";
        echo $html;
    }
    

    include "php/model/libreria.php";
    
    $pass = stripslashes($_POST["pass"]);
    $pass2 = stripslashes($_POST["pass2"]);
    $id = stripslashes($_POST["id"]);
    

    if($pass==""||$pass2==""){
        mostrarError("Ninguno de los campos puede estar vacío. Presione atras.");        
    }
    elseif($pass!=$pass2){
        mostrarError("Las contraseñas ingresadas no concuerdan. Presione atras.");
    }
    else{
        $result = cambiarPassword($id,$pass);
        if($result){
            mostrarError("La contraseña se ha cambiado satisfactoriamente.");
        }
        else{
            mostrarError("Algo ocurrió y no se pudo cambiar la contraseña.");
        }
    }

?>

    </body>
</html>


