
        
<link rel="stylesheet" href="css/footer.css">

<div class="footer">
    <div class="menuFooter" id="btnPerfil">
        <img src="img/sprites/user14.png" alt="Perfil" class="imgFooter"/>
    </div>    
    <div class="menuFooter" id="btnFavoritos">
        <img src="img/sprites/star11.png" alt="Favoritos" class="imgFooter" />
    </div>    
    <div class="menuFooter" id="btnBuscar">
        <img src="img/sprites/search6.png" alt="Buscar" class="imgFooter" />
    </div>    
    <div class="menuFooter" id="btnCategoria">
        <img src="img/sprites/menu10.png" alt="Categoria" class="imgFooter" />
    </div>    
    <div class="menuFooter" id="btnVender">
        <img src="img/sprites/upload8.png" alt="Vender" class="imgFooter" />
    </div>    
</div>


<script src="js/footer.js"></script>