<?php
    session_cache_expire(20);
    session_start();
    if(!isset($_SESSION["loggedIn"])){
        header("location:login.php?page=profile.php");
    }
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/profile.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>

        <script>
<?php

    if(isset($_SESSION["userID"])){
        echo "var idUsuario = ".$_SESSION["userID"].";";
    }

?>
        </script>
<?php
    require_once "header.php" ;
?>
        <div id="contenedorPerfil">
            <div class="informacionUsuario">
                <div id="contenedorFotoUsuario">  
                    <img src="img/profile.jpg" alt="Usuario" id="imgUsuario"/>
                </div>    
                <p class="subtitulo">Nombre</p>
                <p class="valor" id="nombreUsuario">John Doe</p>
                <p class="subtitulo">Email</p>
                <p class="valor" id="emailUsuario">johndoe@aol.fr</p>
                <p class="subtitulo">Seudónimo</p>
                <p class="valor" id="seudonimoUsuario">johnied</p>
                <p class="subtitulo">Celular</p>
                <p class="valor" id="celularUsuario">305 656 4454</p>
            
            </div> 

            <input type="button" value="Editar" id="btnEditar" class="botonPerfil" />

            <form action="doLogout.php">
                <input type="submit" value="Cerrar Sesión" id="btnLogout" class="botonPerfil"/>
            </form>
        
        </div>
<?php
    require_once "footer.php" ;
?>
        
        <script src="js/main.js"></script>
        <script src="js/profile.js"></script>
    </body>
</html>
