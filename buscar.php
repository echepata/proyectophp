<?php
    session_cache_expire(20);
    session_start();
?>

<html>
<head>
	<title></title>
	<meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/normalize.min.css">
	<link rel="stylesheet" type="text/css" href="css/buscar.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<script src="js/vendor/jquery-1.8.0.js"></script>
	<script type="text/javascript" src="js/buscar.js"></script>

</head>
<body>
<?php
    require_once "header.php" ;
?>	
<div class="content">
	 <div class="border"> 	
		<div id ="elementoSugerido">
			<div class='sugeridos' id='div1'></div>
			<div class='sugeridos' id='div2'></div>
			<div class='sugeridos' id='div3'></div>
		</div>	
		<div id="elementoReciente">
			<div class='recientes'></div>
			<div class='recientes'></div>
			<div class='recientes'></div>
		</div>
		<div class="ultimasBusquedas">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div>
</div>
<?php
    require_once "footer.php" ;
?>
</body>
</html>