<?php
    session_cache_expire(20);
    session_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/login.css">
        
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.8.0.js"></script>
    </head>
    <body>
        
<?php
    require_once "header.php" ;
?>
        <script>
            $("#imgLogoHeader").width(0,0);
        </script>
        <div class="register" id="ctnLogin">
            <div id="fondoLogin">
                <img id="logo" src="img/logo.jpg" alt="Logo" />
                <form id="formLogin">
                    <input type="text" value="" placeholder="nombre" class="loginInputText register" /><br class="register">
                    <input type="text" value="" placeholder="seudonimo" class="loginInputText register" /><br class="register">
                    <input type="email" value="" placeholder="username@domain.com" class="loginInputText" id="textEmail" autocomplete="on"/><br>
                    <input type="password" value="" placeholder="contraseña" class="loginInputText" id="textPass" /><br>
                    <input type="password" value="" placeholder="confirmar contraseña" class="loginInputText register" id="confirmPass"/><br class="register">
                    <input type="checkbox" value="y" class="register loginCheckBox" /> <span class="register ">Acepto las condiciones de uso</span> <br class="register">
                    <input type="submit" value="Enviar" class="loginSubmitBtn" /><br>
                    <input type="button" value="Deseo registrarme" class="loginSubmitBtn" id="loginRegisterBtn"/>
                </form> 
            </div>
        </div>
        <div id="ctnLogin">
            <div id="fondoLogin">
                <img id="logo" src="img/logo.jpg" alt="Logo" />
                <form id="formLogin">
                    <h2>BIENVENIDO</h2>
                    <p>Por favor elige tu universidad</p>
                    <select class="loginInputText" id="comboUniversidades">
                        <option>...</option>
                    </select><br>
                    <input type="submit" value="Enviar" class="loginSubmitBtn" /><br>
                </form> 
            </div>
        </div>

<?php
    require_once "footer.php" ;
?>
        
        <script src="js/welcome.js"></script>
    </body>
</html>
